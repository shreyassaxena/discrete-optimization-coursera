# Discrete-optimization-coursera

This folder contains implementations of different discrete optimization techniques primarily from discrete optimization course [1].

[1] https://www.coursera.org/learn/discrete-optimization